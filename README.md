# Cities

Projekt wykonany na podstawie zadania:

> Na pewnej wyspie każde miasto ma bezpośrednie połączenie lotnicze z co najwyżej trzema innymi miastami,
> a z każdego miasta jest możliwość podróży samolotem do każdego innego miasta 
> z co najwyżej jedną przesiadką.
> Ile najwięcej miast może znajdować się na tej wyspie?

Oraz zgodnie z rysunkiem:

![](http://actaeon.one.pl/trash/43189053_2235552439819661_2844020819632324608_n.jpg)

## Co potrzebujemy:
* [CodeBlocks + MinGW](http://www.codeblocks.org/downloads)
* [Git](https://git-scm.com/downloads) (opcjonalnie)

## Jak pobrać i uruchomić?

1. Pobieramy paczkę zip z [LINK](https://gitlab.com/KassVaru/cities/-/archive/master/cities-master.zip)
2. Uruchamiamy program CodeBlocks
3. Wypakowujemy i importujemy nasz projekt do CodeBlocks
4. Wciskamy klawisz F8 na klawiaturze
5. Korzystamy z cudowności programu

### Opcjonalnie
Możemy również wykorzystać pakiet GIT, by pobrać najnowszą wersję programu:
1. W konsoli git/cmd wpisujemy: ``` git clone https://gitlab.com/KassVaru/cities.git ```
2. Reszta taka sama jak wyżej