#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

// Class to create and manage cities
class cities{
public:
    int nr, m1, m2, m3;
    string c_name;
    // Create new city
    void new_city(int a, int b, int c, int d){
        nr=a;
        m1=b;
        m2=c;
        m3=d;
        cout<<"Type name to "<<a<<" city: ";
        cin>>c_name;
    }
    // Show city info
    void show(){
        cout<<"City name: "<<c_name<<endl;
        cout<<"City number: "<<nr<<endl;
    }
};
// Show city info from class
void show_cities(cities city_c[]){
    for(int i=0; i<8; i++){
        city_c[i].show();
    }
}
// Get info what is direct connect from random city
void random_city(int r, cities ct[]){
    for(int i=0; i<8; i++){
        if(ct[i].nr==r){
            int c1=ct[i].m1-1;
            int c2=ct[i].m2-1;
            int c3=ct[i].m3-1;
            cout<<endl<<"You are in: "<<ct[i].c_name<<endl<<"=================="<<endl;
            cout<<"Direct connect to: "<<endl<<ct[c1].c_name<<", "<<ct[c2].c_name<<", "<<ct[c3].c_name<<endl;
        }
    }
}

int main()
{
    // Define time from global
    srand(time(NULL));

    // Create city in project [8]
    cities city[8];
    city[0].new_city(1,8,2,5);
    city[1].new_city(2,1,6,3);
    city[2].new_city(3,2,7,4);
    city[3].new_city(4,3,8,5);
    city[4].new_city(5,4,1,6);
    city[5].new_city(6,5,2,7);
    city[6].new_city(7,6,3,8);
    city[7].new_city(8,7,4,1);
    /* If want to show cities use:
        show_cities(city);
    */
    int random = rand()%8+1; // Random int
    random_city(random, city);
    system("PAUSE");
}
